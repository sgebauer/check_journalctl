check_journalctl
================

This is a simple icinga2 check command for monitoring the systemd journal. It
uses a "cursor" (see `journalctl(1)`) by default to keep track of already
processed messages. Messages can be pre-filtered using most options provided by
`journalctl` (e.g. `--unit`) and then grouped into "warning" and "critical"
messages using a set of regular expressions.

Installation
------------

* Check that you have `bash`, `grep`, and (obviously) `journalctl` installed
* Copy `check_journalctl` to `/usr/local/lib/nagios/plugins/check_journalctl`
* Copy `check_journalctl.conf` to `/etc/icinga2/conf.d/check_journalctl.conf`
* Make sure that icinga2 has read access to the journal. On Debian, run
  `adduser nagios systemd-journal`

Example service object
----------------------

```
apply Service "journal" {
  import "generic-service"
  check_command = "journalctl"

  # The following two options are very useful for services that monitor
  # one-shot events like log messages. They are already set by the journalctl
  # CheckCommand object by default, but might be overwritten by your service
  # template.

  # No re-checks. We want to be notified immediately when something is found.
  max_check_attempts = 1
  # Re-send notifications for every NOT-OK result
  # https://icinga.com/docs/icinga-2/latest/doc/08-advanced-topics/#volatile-services-and-hosts
  volatile = true

  # A lines matching any of these regexes will be considered critical
  vars.journalctl_critical = [ "[Ee]rror" ]
  # A line matching any of these regexes will not be considered critical even if
  # it matches one of the regexes above
  vars.journalctl_critical_ignore = []

  # Same for warnings
  vars.journalctl_warning = [
    "[Ww]arn"
  ]
  vars.journalctl_warning_ignore = [
    "icinga2.*Apply rule.*does not match anywhere"
  ]

  assign where host.name == NodeName
}
```

You might also want to build a separate Notification object for volatile
services in order to get exactly one notification per NOT-OK event. Otherwise,
you will probably get a rather useless recovery notification when icinga stops
finding new problems in the journal.
